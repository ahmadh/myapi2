from django.urls import reverse
from django.test import Client


class TestHomeView:
    def test_success(self):
        response = Client().get(reverse("home"))
        assert response.json() == {'detail': 'Welcome to MyApi1'}
