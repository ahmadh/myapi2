from django.http import HttpResponse, JsonResponse
from django.views.generic import View


class HomeView(View):
    def get(self, request, *args, **kwargs):
        return JsonResponse({'detail': 'Welcome to MyApi1'})